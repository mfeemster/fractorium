fractorium (23.23.8.101-0ubuntu1~pre0) UNRELEASED; urgency=medium

  * Non-maintainer upload.
  * Differences from upstream:
    * update debian build-depends for Qt 6
    * dos2unix debian/rules
    * fix lintian warning about description starting with article
    * remove unused mime type from desktop file (fixes lintian warning)
    * add alternative for _strnicmp on non-Windows
    * update dependencies to OpenEXR 3.1
    * remove executable bit from non-executable files
    * bump debian compat version (allows parallel builds)
    * export raw histogram data
    * line endings in package-linux.sh

 -- Claude Heiland-Allen <claude@mathr.co.uk>  Tue, 05 Dec 2023 14:00:00 +0000

fractorium (22.21.4.2-0ubuntu1) bionic; urgency=low

  * release 22.21.4.2

 -- Matt Feemster <matt.feemster@gmail.com>  Mon, 19 Apr 2021 10:42:17 -0700

fractorium (21.21.4.1-0ubuntu1) bionic; urgency=low

  * release 21.21.4.1

 -- Matt Feemster <matt.feemster@gmail.com>  Wed, 7 Apr 2021 08:12:56 -0700

fractorium (1.0.0.20-0ubuntu1) bionic; urgency=low

  * release 1.0.0.20

 -- Matt Feemster <matt.feemster@gmail.com>  Sun, 11 Apr 2020 10:20:30 -0700

fractorium (1.0.0.19d-0ubuntu1) bionic; urgency=low

  * release 1.0.0.19

 -- Matt Feemster <matt.feemster@gmail.com>  Wed, 18 Mar 2020 19:25:25 -0700

fractorium (1.0.0.19c-0ubuntu1) bionic; urgency=low

  * release 1.0.0.19

 -- Matt Feemster <matt.feemster@gmail.com>  Tue, 17 Mar 2020 08:21:22 -0700

fractorium (1.0.0.19b-0ubuntu1) bionic; urgency=low

  * release 1.0.0.19

 -- Matt Feemster <matt.feemster@gmail.com>  Sun, 15 Mar 2020 23:42:24 -0700

fractorium (1.0.0.19a-0ubuntu1) bionic; urgency=low

  * release 1.0.0.19

 -- Matt Feemster <matt.feemster@gmail.com>  Sun, 15 Mar 2020 15:18:06 -0700

fractorium (1.0.0.19-0ubuntu1) bionic; urgency=low

  * release 1.0.0.19

 -- Matt Feemster <matt.feemster@gmail.com>  Fri, 13 Mar 2020 11:22:33 -0700
 
fractorium (1.0.0.18-0ubuntu1) bionic; urgency=low

  * release 1.0.0.18

 -- Matt Feemster <matt.feemster@gmail.com>  Fri, 31 Jan 2020 20:20:20 -0700
 
fractorium (1.0.0.17a-0ubuntu1) bionic; urgency=low

  * release 1.0.0.17

 -- Matt Feemster <matt.feemster@gmail.com>  Fri, 28 Jun 2019 21:04:38 -0700
 
fractorium (1.0.0.17-0ubuntu1) bionic; urgency=low

  * release 1.0.0.17

 -- Matt Feemster <matt.feemster@gmail.com>  Mon, 24 Jun 2019 20:30:00 -0700
 
fractorium (1.0.0.16-0ubuntu1) bionic; urgency=low

  * release 1.0.0.16

 -- Matt Feemster <matt.feemster@gmail.com>  Thu, 16 May 2019 14:58:06 +0100

fractorium (1.0.0.15-0ubuntu1) bionic; urgency=low

  * release 1.0.0.15

 -- Matt Feemster <matt.feemster@gmail.com>  Mon, 13 May 2019 22:46:22 -0700
 
fractorium (1.0.0.14-0ubuntu1) bionic; urgency=low

  * release 1.0.0.14

 -- Matt Feemster <matt.feemster@gmail.com>  Sat, 20 Oct 2018 13:40:08 -0700

fractorium (1.0.0.13-0ubuntu1) bionic; urgency=low

  * release 1.0.0.13

 -- Matt Feemster <matt.feemster@gmail.com>  Tue, 09 Oct 2018 17:14:30 -0700

fractorium (1.0.0.12b-0ubuntu1) bionic; urgency=low

  * release 1.0.0.12

 -- Matt Feemster <matt.feemster@gmail.com>  Sun, 30 Sep 2018 10:35:26 -0700
 
fractorium (1.0.0.12-0ubuntu1) cosmic; urgency=low

  * release 1.0.0.12

 -- Matt Feemster <matt.feemster@gmail.com>  Sun, 30 Sep 2018 10:35:26 -0700
 
fractorium (1.0.0.11d-0ubuntu1) cosmic; urgency=low

  * release 1.0.0.11

 -- Matt Feemster <matt.feemster@gmail.com>  Sat, 29 Sep 2018 13:26:38 -0700
 
fractorium (1.0.0.11c-0ubuntu1) cosmic; urgency=low

  * release 1.0.0.11

 -- Matt Feemster <matt.feemster@gmail.com>  Thu, 27 Sep 2018 23:08:15 -0700
 
fractorium (1.0.0.11b-0ubuntu1) cosmic; urgency=low

  * release 1.0.0.11

 -- Matt Feemster <matt.feemster@gmail.com>  Thu, 27 Sep 2018 21:59:39 -0700
 
fractorium (1.0.0.11-0ubuntu1) artful; urgency=low

  * release 1.0.0.11

 -- Matt Feemster <matt.feemster@gmail.com>  Thu, 27 Sep 2018 21:12:59 -0700

fractorium (1.0.0.10-0ubuntu1) artful; urgency=low

  * release 1.0.0.10

 -- Matt Feemster <matt.feemster@gmail.com>  Sun, 17 Jun 2018 18:52:58 -0700

fractorium (1.0.0.9-0ubuntu1) artful; urgency=low

  * release 1.0.0.9

 -- Matt Feemster <matt.feemster@gmail.com>  Sun, 13 May 2018 11:20:20 -0700
 
fractorium (1.0.0.8-0ubuntu1) artful; urgency=low

  * release 1.0.0.8

 -- Matt Feemster <matt.feemster@gmail.com>  Fri, 04 May 2018 19:47:30 -0700

fractorium (1.0.0.7g-0ubuntu1) artful; urgency=low

  * release 1.0.0.7

 -- Matt Feemster <matt.feemster@gmail.com>  Fri, 22 Dec 2017 20:09:30 -0800
 
fractorium (1.0.0.7f-0ubuntu1) artful; urgency=low

  * release 1.0.0.7

 -- Matt Feemster <matt.feemster@gmail.com>  Fri, 22 Dec 2017 19:36:15 -0800
 
fractorium (1.0.0.7e-0ubuntu1) artful; urgency=low

  * release 1.0.0.7

 -- Matt Feemster <matt.feemster@gmail.com>  Fri, 22 Dec 2017 17:43:23 -0800

fractorium (1.0.0.7d-0ubuntu1) artful; urgency=low

  * release 1.0.0.7

 -- Matt Feemster <matt.feemster@gmail.com>  Fri, 22 Dec 2017 17:25:44 -0800
 
fractorium (1.0.0.7c-0ubuntu1) artful; urgency=low

  * release 1.0.0.7

 -- Matt Feemster <matt.feemster@gmail.com>  Fri, 22 Dec 2017 16:41:23 -0800
 
fractorium (1.0.0.7b-0ubuntu1) artful; urgency=low

  * release 1.0.0.7

 -- Matt Feemster <matt.feemster@gmail.com>  Thu, 21 Dec 2017 22:17:04 -0800
 
fractorium (1.0.0.7-0ubuntu1) artful; urgency=low

  * release 1.0.0.7

 -- Matt Feemster <matt.feemster@gmail.com>  Thu, 21 Dec 2017 20:31:15 -0800

fractorium (1.0.0.6-0ubuntu1) xenial; urgency=low

  * release 1.0.0.6

 -- Matt Feemster <matt.feemster@gmail.com>  Sun, 03 Sep 2017 13:24:05 -0700
 
fractorium (1.0.0.5c-0ubuntu1) xenial; urgency=low

  * release 1.0.0.5

 -- Matt Feemster <matt.feemster@gmail.com>  Wed, 30 Aug 2017 15:25:31 -0700

fractorium (1.0.0.4-0ubuntu1) xenial; urgency=low

  * release 1.0.0.4

 -- Matt Feemster <matt.feemster@gmail.com>  Sat, 01 Jul 2017 08:54:30 -0700
 
fractorium (1.0.0.3-0ubuntu1) xenial; urgency=low

  * release 1.0.0.3

 -- Matt Feemster <matt.feemster@gmail.com>  Thu, 06 Apr 2017 16:36:00 -0700

fractorium (1.0.0.2b-0ubuntu1) xenial; urgency=low

  * release 1.0.0.2

 -- Matt Feemster <matt.feemster@gmail.com>  Thu, 16 Mar 2017 17:40:26 -0700

fractorium (1.0.0.1-0ubuntu1) xenial; urgency=low

  * release 1.0.0.1

 -- Matt Feemster <matt.feemster@gmail.com>  Wed, 22 Jun 2016 16:36:26 -0700

fractorium (1.0.0.0a-0ubuntu1) xenial; urgency=low

  * release 1.0.0.0a

 -- Gambhiro Bhikkhu <gambhiro.bhikkhu.85@gmail.com>  Sat, 18 Jun 2016 14:31:36 +0100

fractorium (1.0.0.0-0ubuntu1) xenial; urgency=low

  * release 1.0.0.0

 -- Gambhiro Bhikkhu <gambhiro.bhikkhu.85@gmail.com>  Sat, 18 Jun 2016 13:12:15 +0100

fractorium (0.9.9.6-0ubuntu1) xenial; urgency=low

  * release 0.9.9.6

 -- Gambhiro Bhikkhu <gambhiro.bhikkhu.85@gmail.com>  Fri, 06 May 2016 12:58:22 +0100

fractorium (0.9.9.5-0ubuntu1) wily; urgency=low

  * release 0.9.9.5

 -- Gambhiro Bhikkhu <gambhiro.bhikkhu.85@gmail.com>  Mon, 07 Mar 2016 15:26:36 +0000

fractorium (0.9.9.4-0ubuntu1) wily; urgency=low

  * release 0.9.9.4

 -- Gambhiro Bhikkhu <gambhiro.bhikkhu.85@gmail.com>  Wed, 24 Feb 2016 14:16:02 +0000

fractorium (0.9.9.3b-0ubuntu1) wily; urgency=low

  * release 0.9.9.3b

 -- Gambhiro Bhikkhu <gambhiro.bhikkhu.85@gmail.com>  Wed, 20 Jan 2016 17:08:30 +0000

fractorium (0.9.9.2e-0ubuntu1) wily; urgency=low

  * lib symlink fix

 -- Gambhiro Bhikkhu <gambhiro.bhikkhu.85@gmail.com>  Wed, 16 Dec 2015 13:00:40 +0000

fractorium (0.9.9.2b-0ubuntu3) wily; urgency=low

  * include paths and deps for wily

 -- Gambhiro Bhikkhu <gambhiro.bhikkhu.85@gmail.com>  Tue, 15 Dec 2015 13:44:16 +0000

fractorium (0.9.9.2b-0ubuntu1) vivid; urgency=low

  * now it's all qmake

 -- Gambhiro Bhikkhu <gambhiro.bhikkhu.85@gmail.com>  Sat, 05 Dec 2015 20:04:20 +0000

fractorium (0.9.9.2a-0ubuntu1) vivid; urgency=low

  * also install dark theme, build only amd64 for now

 -- Gambhiro Bhikkhu <gambhiro.bhikkhu.85@gmail.com>  Thu, 03 Dec 2015 10:40:53 +0000

fractorium (0.9.9.2-0ubuntu2) vivid; urgency=low

  * libc6-dev for i386 build

 -- Gambhiro Bhikkhu <gambhiro.bhikkhu.85@gmail.com>  Thu, 03 Dec 2015 10:06:40 +0000

fractorium (0.9.9.2-0ubuntu1) vivid; urgency=low

  * Initial release

 -- Gambhiro Bhikkhu <gambhiro.bhikkhu.85@gmail.com>  Tue, 01 Dec 2015 18:23:34 +0000
